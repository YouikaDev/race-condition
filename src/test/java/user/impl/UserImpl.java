package user.impl;

import lombok.Getter;
import user.User;

@Getter
public class UserImpl implements User {
    private volatile double balance;
    private final Object object = new Object();

    public UserImpl(double balance) {
        this.balance = balance;
    }

    @Override
    public double getBalance() {
        return balance;
    }

    @Override
    public void withdraw(double amount) {
        synchronized (object) {
            balance -= amount;
        }
    }

    @Override
    public void deposit(double amount) {
        synchronized (object) {
            balance += amount;
        }
    }
}
