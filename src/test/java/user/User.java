package user;

public interface User {
    double getBalance();
    void withdraw(double amount);
    void deposit(double amount);
}
