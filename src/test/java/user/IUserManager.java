package user;

public interface IUserManager {
    void transfer(User from, User to, double amount) throws InterruptedException;
}
