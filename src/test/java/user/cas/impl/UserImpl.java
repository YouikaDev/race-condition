package user.cas.impl;

import com.google.common.util.concurrent.AtomicDouble;
import user.User;

public class UserImpl implements User {

    private final AtomicDouble balance;

    public UserImpl(double balance) {
        this.balance = new AtomicDouble(balance);
    }

    public double getBalance() {
        return balance.get();
    }

    public void withdraw(double sum) {
        boolean success = false;
        while (!success) {
            double value = getBalance();
            double newValue = value - sum;

            success = balance.compareAndSet(value, newValue);
        }
    }

    public void deposit(double sum) {
        boolean success = false;
        while (!success) {
            double value = getBalance();
            double newValue = value + sum;

            success = balance.compareAndSet(value, newValue);
        }
    }
}
