package user.sync;

import user.IUserManager;
import user.User;

public class UserManager implements IUserManager {

    @Override
    public synchronized void transfer(User from, User to, double amount) {
        System.out.println("before balance from: " + from.getBalance() + " to: " + to.getBalance());

        from.withdraw(amount);
        to.deposit(amount);

        System.out.println("after balance from: " + from.getBalance() + " to: " + to.getBalance());
    }
}
