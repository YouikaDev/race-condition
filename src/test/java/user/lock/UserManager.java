package user.lock;

import user.IUserManager;
import user.User;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class UserManager implements IUserManager {
    private final Lock lock;
    private final Condition condition;

    public UserManager() {
        lock = new ReentrantLock();
        condition = lock.newCondition();
    }

    @Override
    public void transfer(User from, User to, double amount) throws InterruptedException {
        lock.lock();
        try {
            while (from.getBalance() < amount) condition.await();

            System.out.println("before balance from: " + from.getBalance() + " to: " + to.getBalance());

            from.withdraw(amount);
            to.deposit(amount);

            System.out.println("after balance from: " + from.getBalance() + " to: " + to.getBalance());

            condition.signalAll();
        } finally {
            lock.unlock();
        }
    }
}
