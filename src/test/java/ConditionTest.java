import org.junit.jupiter.api.Test;
import user.IUserManager;
import user.User;
import user.impl.UserImpl;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ConditionTest {

    private final int threadCount = 10;

    @Test
    public void testTransferLock() throws NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException, InterruptedException {
        ExecutorService service = Executors.newFixedThreadPool(threadCount);

        List<User> users = new ArrayList<>();

        for (int i = 0; i < 2; i++) {
            users.add(UserImpl.class.getConstructor(double.class).newInstance(100));
        }

        IUserManager userManager = new user.lock.UserManager();

        for (int i = 0; i < threadCount; i++) {
            service.execute(() -> {
                for (int j = 0; j < threadCount; j++) {
                    try {
                        userManager.transfer(users.get(0), users.get(1), 1);
                        Thread.sleep((long) (j * Math.random()));
                    } catch (InterruptedException e) {
                        throw new RuntimeException(e);
                    }
                }
            });
        }

        service.awaitTermination(3, TimeUnit.SECONDS);

        assertEquals(users.get(1).getBalance(), 200);
        assertEquals(users.get(0).getBalance(), 0);
    }

    @Test
    public void testTransferSynchronized() throws NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException, InterruptedException {
        ExecutorService service = Executors.newFixedThreadPool(threadCount);

        List<User> users = new ArrayList<>();

        for (int i = 0; i < 2; i++) {
            users.add(UserImpl.class.getConstructor(double.class).newInstance(100));
        }

        IUserManager userManager = new user.sync.UserManager();

        for (int i = 0; i < threadCount; i++) {
            service.execute(() -> {
                for (int j = 0; j < threadCount; j++) {
                    try {
                        userManager.transfer(users.get(0), users.get(1), 1);
                        Thread.sleep((long) (j * Math.random()));
                    } catch (InterruptedException e) {
                        throw new RuntimeException(e);
                    }
                }
            });
        }

        service.awaitTermination(3, TimeUnit.SECONDS);

        assertEquals(users.get(1).getBalance(), 200);
        assertEquals(users.get(0).getBalance(), 0);
    }

    @Test
    public void testTransferCAS() throws NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException, InterruptedException {
        ExecutorService service = Executors.newFixedThreadPool(threadCount);

        List<User> users = new ArrayList<>();

        for (int i = 0; i < 2; i++) {
            users.add(user.cas.impl.UserImpl.class.getConstructor(double.class).newInstance(100));
        }

        IUserManager userManager = new user.cas.UserManager();

        for (int i = 0; i < threadCount; i++) {
            service.execute(() -> {
                for (int j = 0; j < threadCount; j++) {
                    try {
                        userManager.transfer(users.get(0), users.get(1), 1);
                        Thread.sleep((long) (j * Math.random()));
                    } catch (InterruptedException e) {
                        throw new RuntimeException(e);
                    }
                }
            });
        }

        service.awaitTermination(3, TimeUnit.SECONDS);

        assertEquals(users.get(1).getBalance(), 200);
        assertEquals(users.get(0).getBalance(), 0);
    }
}
